import re
import datetime as D
from bs4 import BeautifulSoup

DOMAIN = "https://ab.onliner.by/"

def default(r):
    raw_car_table = r.html.find('table')[0].html
    soup = BeautifulSoup(raw_car_table, 'html.parser')
    car_rows = soup.select('.carRow')
    data = []

    for row in car_rows:
        price = row.select('.cost .price-primary')[0].text
        price = int(price.replace(' ', ''))

        car_name = row.select('.txt h2 strong')[0].text.replace('\n', '')
        car_name = re.sub(' +', ' ', car_name)
        car_name = car_name.lstrip()
        car_name = car_name.rstrip()
        year = int(row.select('.txt span.year')[0].text)
        mileage = row.select('.txt span.dist strong')[0].text
        mileage = mileage.replace(' ', '')
        try:
            mileage = int(mileage)
        except:
            print('Parsing error.')

        car_link = row.select('.txt h2 a')[0].get('href')
        now = D.datetime.now().isoformat()
        car_data_item = dict(price=price, car_name=car_name, year=year, mileage=mileage, car_link=car_link, date=now, domain=DOMAIN)
        data.append(car_data_item)
    return data
