def default (parser_name):
    PARSERS_ENUM = {
        'onliner': 'https://ab.onliner.by',
    }

    return PARSERS_ENUM[parser_name]
