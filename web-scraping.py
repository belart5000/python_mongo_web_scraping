# Configs (temporary implementation)
import trigger_params

# Other modules
import pymongo
from requests_html import HTMLSession

# helpers
import parsers_mapper

# Parsers
from parsers.onliner import default as onliner

CLIENT = pymongo.MongoClient('localhost')
DB = CLIENT.get_database('cars_test_db')
CONFIG = trigger_params.default()

CURRENT_CLIENT_COLLECTION_NAME = CONFIG['login']

try:
    DB.create_collection(CURRENT_CLIENT_COLLECTION_NAME)
except:
    print('Collection ', CURRENT_CLIENT_COLLECTION_NAME, 'is exist')

print('Getting HTML-data...')
current_parser = False
current_car_list = list(DB[CURRENT_CLIENT_COLLECTION_NAME].find())
parsed_cars_raw_list = []

for parser in CONFIG['params']['system']['parsers']:
    current_url = parsers_mapper.default(parser)
    session = HTMLSession()
    r = session.get(current_url)
    r.html.render()
    parsed_cars_raw_list = eval(parser)(r)

filtered_car_list = []

for car in parsed_cars_raw_list:
    is_car_exist = False

    for current_car in current_car_list:
        if current_car['car_link'] == car['car_link']:
            is_car_exist = True

    if is_car_exist == False:
        filtered_car_list.append(car)

print(filtered_car_list)

if len(filtered_car_list):
    DB[CURRENT_CLIENT_COLLECTION_NAME].insert_many(filtered_car_list)
print('Parsing was successfully finished!')


