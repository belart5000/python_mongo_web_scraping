def default ():
    config = {
        'login': 'admin',
        'password': 'myPass1',
        'emails': ['belart21@tut.by'],
        'params': {
            'system': {
                'scriptExecutionInterval': '3600',
                'parsers': ['onliner']
            }
        }
    }

    return config
